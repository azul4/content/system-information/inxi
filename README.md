# inxi

Full featured CLI system information tool

https://smxi.org/docs/inxi.htm

https://github.com/smxi/inxi

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/system-information/inxi.git
```
